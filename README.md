flexi.chat - Flexible videoconferencing formats
----
[flexi.chat](flexi.chat) is an app that allows you to run videoconference events with different speaking formats as the event unfolds.

Motivation
---
In-person events rarely have the same format throughout:

* When you arrive at eg a meetup, you chit-chat and meet new people for a little bit before and after the main talks happen.
* At conferences, there are both single speaker sessions and panels.
* In some meetings you want to have participants take turns speaking, or leave some time for questions at the end.
* Sometimes you want to have eg 3 rooms, vote on a topic as the event unfolds, and then dedicate the rooms to group conversations about the most popular topics.

The goal of flexi.chat is to enable a single event to support and switch between all of these different formats.

You'll be able to describe your event with a schedule of different formats, which might look like:
```
# start_at: 6_30PM EST
events:
    - Random pairs
      - duration: 20 minutes
      - rotate_every: 5 minutes
    - Main Speaker
      - duration 30 minutes
      - speaker_id: @main_speaker
    - Random Groups
      - group_size: 4
      - duration: 5 minutes
```

The code is written with format extensibility in mind. It will be really easy to make your custom formats that have their own behaviour.

Later on there will be the ability to have multiple rooms people can choose between, and to have formats work over multiple rooms. For example a tournament mode where the placement of players in each room depends on who wins each game.

Documentation: Existing Formats
---
Formats are being developed as we think them up and as we need them. At the moment, we have the following:

### SpeakingCircle
Everyone is in the same room and gets to speaks for the same interval (everyone else gets muted). If users join after the format starts, they get added to the end of the list. If a user re-joins after speaking, they don't get re-added. The main speaker also has the option to end their turn early.

### ShufflePairs
Everyone is split into pairs, and then every (eg) 5 minutes the pairs are rotated so that everyone ends up being paired with everyone else.

### Lobby
Everyone goes into a single room, called Lobby.

Contributions in the format of clearer names for these formats would be greatly appreciated.

##### Formats to add:
* MainSpeaker/Panel: A set of main speakers is designated, either ahead of time in the schedule, or by the event host at runtime. Everyone else gets muted.
* Questions: A way for users to queue up to ask questions, and then the speakers answer them.
* Unconference/topic voting: A way to have event participants propose, then vote on conversation topics, and then for rooms to be created for them.
This one depends on the ability to have multiple rooms in an event, which isn't thought out yet.

Documentation: Concepts
---

* Event
* Schedule
* Runner
* Format

## Event
Events are the reason we make Flexichat. An event has Users which participate in it,
mostly through videochat (but also each format can add extra UI components), and who participate in the
different Formats that happen over time, according to the Schedule.

## Schedule
An Event has a Schedule, which is information about how the Event will proceeed.
ATM it consists of a of a start time and a list of `[Format, duration, formatParams]` items, where
* `Format` is the specific format to run
* `duration` is how long to run it for
* `formatParams` is format specific configuration.
For example, the `ShufflePairs` format has a parameter for how long people should be paired together before
rotating the pairs.

We will be adding a JSON-Schema document for formats soon.

## Runner
Is entirely part of Schedule ATM.

The Schedule Runner will be the logic that evaluates which Format to run, as well as keeping track of
and communicating to users. The idea is that a Schedule will be a declaration of the formats that should
 happen and the Runner will load/unload them, and give them their dependencies (websockets and db access mostly).

## Format
A definition of how users should interact.
A Format tells each User's Browser which Jitsi Room to go join.

It can also send each Browser status info as it goes along.

If you make a format, you should add a React component to display its metadata under `components/statuses/YourFormatName.js`.

Later on Formats will be able to define their own UI. For example, if we are pairing users together, the format could look up user
bios and insert it on the meetin room page. Or a format could have several rooms, and it could set styling information for that room.


Documentation: High Level Implementation
---
The application is a Next.js application with a custom Typescript backend.
This means the front-end is entirely React, with Nextjs functionality for moving between pages, and `socket.io` as the websocket communication
layer to implement the format flexibility.

* Front-end: React SPA, with socketio for real-time format information
* WebRTC: At the moment we use Jitsi Meet as our video provider. It seems fine, and has many features, so no use re-inventing the wheel.
* * However in the future we'll probably have to host our own Jitsi Meet Server.
* Back-end: Nodejs in typescript. With
* socket.io as the communication layer

These components were picked because they are all pretty simle and stable.

The hosting is currently on Heroku.


Documentation: Backend Architecture
---
The backend uses a "custom server" in Next.js, which let us control the Expressjs server. The only tweak we've done so far
was adding the `socketio` integration.

At this moment we have a hard-coded Event continuously running.

The event's Schedule builds the `Format` components, and handles users joining the socketio channel.

As users join the currently running `Format` is notified and it
can choose how to update itself.

The `Format` chooses which rooms to place each user in, and then sends them a `set-user-room` socketio message to
that the frontend uses.

The frontend has a React component with a prop for `roomName`, and reconnects to jitsi when that changes.

Formats dynamically move people betwen rooms, and sometimes they send extra status information, or facilitate
Jitsi specific changes. For example, the `ShuffleSpeaker` format gives everyone a minute to be the main speaker
and sends events to the other users that mute their Jitsi Meet instance.

Documentation: Adding a custom format
---
Look at `server/formats/RandomRooms.ts` for a place to start.
The main idea is to extend the `Format` base class, and then you have the option of implementing all the following
lifecycle methods:

* isStarting (required)
* isEnding
* userJoined
* userLeaving

You can use `this.setUserRoom` to place a user in a room, and
`this.sendUserMessage` to send a more generic message with a subject that the frontend can interpret.

ATM you need to add your format to `Schedule.loadedFormats` in `server/Schedule.ts`.

On the frontend, you can add a custom React component under `FormatStatuses/YourFormat` to display your format however you want.
You can define the `get status()` JS getter on your format, and call `this.broadcastStatus` to update the display of this component
to all users.

