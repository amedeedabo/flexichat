import {Event} from "../server/interfaces"
import {RRule} from "rrule"

var events_db : Event[] = (() => [
	{
		starts_at: "May 22 2020 11:00:00 GMT-0400 (Eastern Daylight Time)",
		id: "standup",
		name: "Standup",
		rrule: "RRULE:FREQ=DAILY;INTERVAL=1",
		blocks: [
			{ name: "Lobby", duration: 2*60 },
			{ name: "SpeakingCircle", duration: 5*2*60, params: { intervalSecs: 120, loop: false } },
			{ name: "ShufflePairs", duration: 5*2*60, params: { intervalSecs: 60, loop: false } },
			{ name: "Lobby", duration: 7*60 },
		]
	},
	{
		starts_at: new Date(new Date().getTime() + 10*1000).toString(),
		id: "hhw",
		name: "Hungry Hungry Women",
		blocks: [
			{ name: "Lobby", duration: 30},
			{ name: "ShufflePairs", duration: 5*2*60, params: { intervalSecs: 10, loop: false } },
			{ name: "SpeakingCircle", duration: 5*60, params: { intervalSecs: 5, loop: false } },
			{ name: "SpeakingCircle", duration: 5*2*60, params: { intervalSecs: 120, loop: false } },
			{ name: "Lobby", duration: 7*60 },
		]
	},
	{ //Real event
		starts_at: "May 25 2020 19:00:00 GMT-0400 (Eastern Daylight Time)",
		id: "hungry-hungry-women",
		name: "Hungry Hungry Women",
		blocks: [
			{ name: "Lobby", duration: 5*60},
			{ name: "SpeakingCircle", duration: 6*120, params: { intervalSecs: 120, loop: false } },
			{ name: "ShufflePairs", duration: 5*5*60, params: { intervalSecs: 5*60, loop: false } },
			{ name: "Lobby", duration: 20*60 },
		]
	},
	{ //Real event
		starts_at: "May 26 2020 18:00:00 GMT-0400 (Eastern Daylight Time)",
		id: "the-boys",
		name: "The Boys",
		blocks: [
			{ name: "Lobby", duration: 5*60},
			{ name: "SpeakingCircle", duration: 6*120, params: { intervalSecs: 120, loop: false } },
			{ name: "ShufflePairs", duration: 5*5*60, params: { intervalSecs: 5*60, loop: false } },
			{ name: "Lobby", duration: 20*60 },
		]
	},
	{ //Real event
		starts_at: "May 25 2020 18:10:00 GMT-0400 (Eastern Daylight Time)",
		id: "dabo",
		name: "d'Aboville Get-Together",
		blocks: [
			{ name: "Lobby", duration: 5*60},
			{ name: "SpeakingCircle", duration: 6*120, params: { intervalSecs: 120, loop: false } },
			{ name: "ShufflePairs", duration: 5*5*60, params: { intervalSecs: 5*60, loop: false } },
			{ name: "Lobby", duration: 20*60 },
		]
	},
])()
export function getAllEvents() : Event[] {
	return events_db
}
export const updateEvent = (id: string, updated_event: any) =>  {
	let idx = events_db.findIndex(e => e.id == id);
	if (idx == -1) {
		console.log(`DB: asked to update event ${id} but no event was found with that id.`)
		return
	}
	events_db[idx] = {...events_db[idx], ...updated_event};
	console.log(`Updated event ${id}.`)
	console.log(updated_event)
}
export async function getEventById(eventId: string) : Promise<any | undefined> {
	return getAllEvents().find(e=> e.id === eventId)
}
function updateRecurringEvents() {
	const now = new Date()
	for (const event of getAllEvents()) {
		if(event.rrule && new Date(event.starts_at) < now) {
			updateEvent(event.id, {"starts_at": RRule.fromString(event.rrule).after(now).toString()})
		}
	}
}
export async function getFutureEvents() : Promise<any[]> {
	updateRecurringEvents();
	const now = new Date();
	return getAllEvents().filter(conf=> new Date(conf.starts_at) >= now);
}
