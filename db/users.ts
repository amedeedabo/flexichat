import {User} from "../server/interfaces"
import randomAnimal from "random-animal-name";

var users_db : User[] = [
	{
	    id: 1,
	    email: "test@example.com",
		alias: "No animal",
	}
]
const save_user = (user: User) => {
	console.log("Pushing user onto db")
	users_db.push(user)
	console.log(users_db)
}

const getNewUserId = () => users_db.slice(-1)[0].id + 1;

export const createAnonUser = () : User => {
	const new_user = {
		id: getNewUserId(),
		alias:  randomAnimal(),
		email: '',
	}
	save_user(new_user)
	return new_user;
}
