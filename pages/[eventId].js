import Error from 'next/error'
import Head from 'next/head'
import Jitsi from '../components/Jitsi'
import Layout from "../components/Layout";
import Schedule from '../components/Schedule.js'
import FormatStatus from '../components/FormatStatus.js'
import style from "../components/Event.module.scss";
import io from 'socket.io-client'
import {Component} from 'react'
import {getEventById} from "../db/events"
import Countdown from "../components/Countdown"
import {sessionOptions} from "../lib/sessions"
import {createAnonUser} from "../db/users"

function toggleAudioIfSpeaker(jitsi, userId, speakerId) {
	if (!jitsi) {
		return
	}
	jitsi.isAudioMuted().then((muted => {
		if (muted == (userId == speakerId)) {
			console.log(`Toggling audio`);
			jitsi.executeCommand('toggleAudio');
		}
	}))
}
//Note: This is messy, but we've been working without an ORM or a DB or any concept of MVC until now.
//I think it's cool how far we're getting without having that tech set up yet.
export async function getServerSideProps({req, res, params}) {
	const sched  = await getEventById(params.eventId)
	console.log("Found schedule")
	console.log(sched)
	if (!req.session.user) {
		console.log("No user in session, creating a new one")
		req.session.user = createAnonUser();
		req.session.save();
	}
	//TODO check the user has permission to read the event
	let ret = {props: {}}
	if (!sched) {
		ret.props.errorCode = '404'
	} else {
		ret.props.sched = sched
		ret.props.user = req.session.user
	}
	return ret
}
export default class Medium extends Component {
	constructor(props) {
		super(props)

		this.state = {
			uid: '',
			presence: {},
			room_id: 'lobby'
		};
	}

	componentDidMount() {
		if(this.props.sched == undefined) {
			return;
		}
		console.log("Connecting to socketio")
		console.log(this.props)
		this.socket = io(`/${this.props.sched.id}`)
		console.log(this.socket)
		this.socket.on('message', data => {
			console.log("received message");
			console.log(data);
			this.setState({ hello: data.body });

		});
		this.socket.on('set-alias', data => {
			this.setState({ alias: data.alias });
		});
		this.socket.on('set-room', data => {
			this.setState({ room_id: data.room_id });
		});
		this.socket.on('presence', presence => {
			this.setState({ presence });
		});
		this.socket.on('format-status', formatInfo => {
			this.setState({ formatInfo });
		});
		this.socket.on('set-main-speaker', (speakerId => {
			console.log(`Main speaker is now ${speakerId}`);
			toggleAudioIfSpeaker(this.jitsi, this.props.user.id, speakerId);
		}).bind(this));
	}
	onJitsiLoad(api) {
		this.jitsi = api;
		//Bad to add format specific logic here but ey, we'll have to do more work later to
		//make it generic.
		console.log("jitsi loaded")
		console.log(`Format info name is ${this.state.formatInfo.name}`)
		if (this.state.formatInfo.name == 'SpeakingCircle') {
			toggleAudioIfSpeaker(this.jitsi, this.props.user.id, this.state.formatInfo.mainSpeaker);
		}
	}

	render() {
		let { sched, errorCode, user} = this.props;
		let notStarted = (this.state.formatInfo == undefined);
		let room = `flexichat-${sched?.id}-${this.state.room_id}`;
		if (errorCode) {
			return <Error statusCode={errorCode} />
		} else {
			return (
				<Layout>
					<Head>
						<script src='/jitsi_external_api.js'></script>
					</Head>
					<div className={style.Content}>
						<h1 className={style.title}>{sched.name}</h1>
						<h4> Welcome, {user.alias}</h4>
						<div className={style.Container}>
							<Schedule sched={sched} />
							{notStarted ? <EventCountdown sched={sched} /> : (
								<>
									<FormatStatus room_id={this.state.room_id} {...this.state.formatInfo} presence={this.state.presence} user={user} socket={this.socket}/>
									<Jitsi name={user.alias} style={{flexGrow: "2"}} onJitsiLoad={this.onJitsiLoad.bind(this)} room={room} />
								</>)}
						</div>
					</div>
				</Layout>
			)
		}
	}
}

const EventCountdown = (props) => !props.sched?.showCountdown ?
		(<div>This event hasn't started yet. <br/>Countdown: <Countdown end={new Date(props.sched?.starts_at)}/></div>) :
		(<div>This event hasn't started yet.</div>)