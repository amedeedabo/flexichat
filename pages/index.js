import Layout from "../components/Layout";
import {Component } from 'react'
import { Button } from 'reactstrap'
import NavBar from "../components/NavBar";
import style from "./index.module.scss";

const Panel = props => (
    <div className={props.className || style.Panel} style={props.style}>
		{props.children}
    </div>
  );

export default class Medium extends Component {
	render() {
		return (<Layout>
    <Panel style={{backgroundImage: "linear-gradient(20deg, rgb(219, 112, 147), rgb(218, 163, 87))"}}>
        <div className={style.PanelContent}>
			<div className={style.cover}>
				<h1 class="cover-heading">Have more interesting remote video events.</h1>
				<p class="lead">flexi.chat is an app where you can have group calls with flexible schedules and interactive speaking formats.</p>
				<img className={style.MainScreenShot} src="/flexishot.jpg" alt="A screenshot of flexichat, showing the ability to have a schedule in a videochat"/>
			</div>
		</div>
	</Panel>
    <Panel background={"white"} className={style.panel2}>
		<div className={style.panel3}>
			<h2>Having a potluck, a family reunion, a meetup, or a conference isn't the same on a videochat.</h2>
			<h2>In real life, rooms have different corners for people to be in, and it's easy to take turns.</h2>
			<h2>flexi.chat helps this by scripting a videoconference so that events can be more fluid.</h2>
		</div>
	</Panel>
    <Panel style={{backgroundColor: "lightpink"}}>
        <div className={style.panel3}>
			<div className={style.Example}>
				<h2>Examples of things you can do:</h2>
				<ul>
					<li>Have an introduction section to a meeting where everyone introduces themselves one by one. Everyone else gets muted.</li>
					<li>Run an online meetup where participants are randomly paired every 5 minutes before the main talks.</li>
					<li>At the end of the talks, queue up questions.</li>
					<li>Run a friend reunion where everyone is paired up with everyone over the course of the event.</li>
					<li>Vote on topics as the event unfolds, and then dedicate rooms to group conversations about the most popular topics.</li>
					<li>...etc!</li>
				</ul>
			</div>
		</div>
	</Panel>
    <Panel styleProps={{backgroundImage: "linear-gradient(20deg, rgb(219, 112, 147), rgb(218, 163, 87))"}} className={style.howitworks}>
        <div className={style.panel3}>
		<h2>How it works</h2>
		<div style={{display:"flex", flexDirection: "column", alignItems:"start" }}>
			<h4>1. Choose your event schedule</h4>
			<h5 style={{paddingLeft:"2rem"}}>Choose how you want people to interact. Combine the different formats into blocks of time, and tweak them how you want them.</h5>
			<h5 style={{paddingLeft:"2rem"}}>Or use our pre-written templates.</h5>
			<h4>2. Schedule your event</h4>
			<h5 style={{paddingLeft:"2rem"}}>flexi.chat can send out email invites if your event, or give you a link to add to Google calendar.</h5>
			<h4>3. Participate and keep the event going smoothly</h4>
			<h5 style={{paddingLeft:"2rem"}}>flexi.chat has a Host View, where you can see your event and tweak the schedule, make announcements, or add/remove people.</h5>
		</div>
		</div>
	</Panel>
	<Panel style={{backgroundColor:"#ffb260"}}>
		<div className={style.panel3}>
		<h1>Open Source</h1>
		<p>Join us  by contributing new ideas, bugfixes, designs, formats, or code at Gitlab!</p>
		</div>
	</Panel>
    <NavBar />
	</Layout>
		)
	}
}

