import {Format} from './Format'
/*
Everyone is in the same room.
*/
export default class Lobby extends Format {
    get status() {
        return {
            name: this.constructor.name,
            users: Object.keys(this.users),
        }
    }
    isStarting() {
        Object.keys(this.users).map(id=> this.setUserRoom(id, "lobby"));
    }
    userJoined(userId) {
        this.setUserRoom(userId, "lobby")
        this.broadcastStatus();
    }
    userLeaving(_userId) {
        this.broadcastStatus();
    }
}