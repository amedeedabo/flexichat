import {Users, User, UserId} from '../interfaces';
export enum PlayState {
    NOT_STARTED,
    PLAYING,
    ENDED
}
/* Our base class for formats.
Tries to handle as many common concerns (user management and communication)
as possible while leaving hooks open in the right lifecycle methods.

Takes commands from the Scheduler, which calls things like
addUser(), removeUser(), start() and end().
This Format then does any busywork needed and calls it-s subclass
userJoined(), userLef(), isStarting() and isEnding(), respectively.
*/
export abstract class Format {
    users: Users = {};
    io: any;
    sockets: any = {};
    resolvePromise: Function | undefined;
    active_state :  PlayState = PlayState.NOT_STARTED;

    constructor({ioServer}) {
        this.io = ioServer;
    }
    abstract get status(): Object;

    addUser(user:User, socket) {
        if (!user || !socket) {
            return;
        }
        this.sockets[user.id] = socket;
        this.users[user.id] = user
        this.sendUserStatus(user.id, this.status);
        this.userJoined(user.id);
    }
    removeUser(user_id) {
        this.userLeaving(user_id);
        delete this.users[user_id];
    }
    start(users, sockets) {
        this.users = users;
        this.sockets = sockets
        console.log(`${this.constructor.name}: Starting with ${Object.keys(this.users).length} users`);
        this.broadcastStatus();
        this.isStarting(users);
        this.active_state = PlayState.PLAYING;
        return new Promise((resolve, _reject) => {
            this.resolvePromise = resolve;
        });
    }
    //Ends the format if it is active.
    end() {
        if (this.active_state != PlayState.PLAYING) {
            return;
        }
        console.log(`${this.constructor.name} is ending.`);
        if (!!this.isEnding) {
            this.isEnding();
        }
        this.active_state = PlayState.ENDED;
        if (this.resolvePromise != undefined) {
            this.resolvePromise();
        }
    }
    /*Possible subclassable functions */

    userJoined(_user_id: UserId) {}
    userLeaving(_user_id: UserId) {}
    abstract isStarting(_users: Users)
    isEnding() {}

    /*User communication concerns
    Might get split off into its own object later once we figure out what we need:
    */
    setUserRoom(userId, roomNumber) {
        console.log(`Giving user ${userId} room id ${roomNumber}`);
        this.sendMessage(userId, 'set-room', {'room_id': roomNumber});
    }
    sendUserStatus(userId, status) {
        this.sendMessage(userId, 'format-status', status);
    }
    broadcastStatus() {
        let status = this.status;
        for (const id in this.users)  {
            this.sendUserStatus(id, status);
        }
    }
    //The only two methods that know about socketio atm. Subject to change.
    //The idea will be that Format will be given some kind of Transport object
    //that will have a way to send a user message, +broadcast to a subset of users.
    //I'm not sure how to abstract it yet.
    //I guess the Transport will have a map between user-id and whatever connection
    //it uses, and should be able to figure that out. So I guess instead of being
    //in the the this.users list it will be in a separate list, and this.users will
    //hold more application specific information
    sendMessage(userId, subject, message) {
        if(this.sockets[userId]) {
            this.sockets[userId].emit(subject, message);
        } else {
            console.error(`No socket for user ${userId}, can't send message with subject ${subject}`)
        }
    }
    broadcastMessage(subject, message) {
        this.io.emit(subject, message);
    }
}
