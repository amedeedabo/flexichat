import {Format} from './Format'
import pairsCalc from 'robin-js'
/*
Everyone is split into pairs, and then the pairs are rotated over time
so that everyone ends up talking to everyone.
*/
export default class ShufflePairs extends Format {
    intervalSecs: number;
    intervalObj: ReturnType<typeof setInterval> | undefined;
    intervalIdx: number = -1;
    matchList : [string, string][];

    get status() {
        return {
            name: this.constructor.name,
            currentMatches: this.matchList[this.intervalIdx],
            interalSecs: this.intervalSecs,
        }
    }

    constructor(params: any) {
        super(params)
            this.intervalSecs = params.intervalSecs;
            this.matchList = [];
    }
    isStarting() {
        this.matchList = pairsCalc(Object.keys(this.users));
        this.intervalObj = setInterval(this.nextRound.bind(this), this.intervalSecs * 1000);
        this.nextRound();
    }
    isEnding() {
        if(this.intervalObj) {
            clearInterval(this.intervalObj);
        }
    }
    nextRound() {
        this.intervalIdx++;
        console.log(`ShufflePairs: next round, now doing interval ${this.intervalIdx}`);
        if(this.intervalIdx < this.matchList.length) {
            this.mapUsers();
        } else {
            console.log("ShufflePairs: have cycled through all the pairs, ending now.");
            this.end();
        }
    }
    mapUsers() {
        let i = 0;
        this.broadcastStatus();
        for (const pair of this.matchList[this.intervalIdx]) {
            this.setUserRoom(pair[0], i);
            if (pair.length > 1) {
                this.setUserRoom(pair[1], i);
            } else {
                this.sendMessage(pair[0], 'user-message', "You're by yourself this round! Take a breather.")
            }
            i++;
        }
    }
}