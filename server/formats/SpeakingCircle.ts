import {Format} from './Format'
import {shuffle} from './util'
import { UserId } from 'server/interfaces';
/*
Everyone is in the same room and speaks one after the other (with optional loop).
*/
export default class SpeakingCircle extends Format {
    loop: Boolean;
    speakerIdx: number = -1;
    hasSpoken: UserId[] = []; //Users who have already spoken.
    speakingOrder: UserId[] = []; //The full list of speakers. May contain users who have left.
    speakerEndsAt: number = -1;
    intervalSecs: number;
    timeout: ReturnType<typeof setInterval> | undefined;

    constructor(params: any) {
        super(params);
        let {intervalSecs, loop} = params;
        this.intervalSecs = intervalSecs;
        this.loop = loop;
    }

    get currentSpeaker() {
        return this.speakingOrder[this.speakerIdx]
    }
    get status() {
        return {
            name: this.constructor.name,
            speakingOrder: this.speakingOrder,
            hasSpoken: this.hasSpoken,
            speakerIdx: this.speakerIdx,
            mainSpeaker: this.currentSpeaker,
            interval: this.intervalSecs,
            loop: this.loop,
            speakerEndsAt: this.speakerEndsAt,
        }
    }

    userJoined(user_id: UserId) {
        //If the user is re-joining and has already spoken, we don't add them to the speaking list
        console.log(`user ${user_id} joined. Has spoken is`)
        console.log(this.hasSpoken)
        console.log(`this.hasspoken includes: ${user_id} is ${this.hasSpoken.includes(user_id)}`)
        if (!this.hasSpoken.includes(user_id)) {
            console.log(`Adding user ${user_id} because hasSpoken doesn't have it`);
            this.speakingOrder.push(user_id)
            }
        this.setUserRoom(user_id, 0);
        this.broadcastStatus();
    }
    userLeaving(user_id: UserId) {
        //For now, if a user is the main speaker and leaves, we move on and then count as
        //having spoken.
        if(user_id == this.currentSpeaker) {
            this.nextSpeaker()
        } else {
            //If the user hasn't spoken yet, remove them from the list of future speakers
            const index = this.speakingOrder.indexOf(user_id);
            if (index > -1 && index > this.speakerIdx) {
                this.speakingOrder.splice(index, 1);
            }
            this.broadcastStatus();
        }
    }
    isStarting() {
        Object.values(this.users).map((user) => this.setUserRoom(user.id, 0));
        this.speakingOrder = shuffle(Object.values(this.users).map(u=> u.id));
        this.nextSpeaker();
    }
    isEnding() {
        if(this.timeout) {
            clearTimeout(this.timeout);
        }
    }

    nextSpeaker() {
        console.log("Circle: next speaker");
        console.log(this.status);

        if(this.speakerIdx >= 0) {
            this.hasSpoken.push(this.currentSpeaker);
        }

        if (!(this.speakingOrder.length > 0)) {
            console.log(`Circle: Asked for next speaker when there are no speakers.`);
            return
        }
        if (this.hasSpoken.length >= this.speakingOrder.length || this.speakerIdx >= (this.speakingOrder.length-1) ) {
            if (!this.loop) {
                console.log(`Circle: Everyone has spoken, we are done.`);
                this.end();
                return
            } else {
                this.speakingOrder = shuffle(this.speakingOrder)
                console.log(`Circle: Everyone has spoken, restarting the loop. New speaking order is ${this.speakingOrder}`);
                this.hasSpoken = [];
                this.speakerIdx = -1;
            }
        }
        this.speakerIdx +=  1;
        const newSpeaker = this.speakingOrder[this.speakerIdx];
        if(newSpeaker) {
            console.log(`Circle: Making ${newSpeaker} the main speaker`);
            this.setUserSpeaker(newSpeaker);
        } else {
            console.log(`Got undefined for speakingOrder [ ${this.speakerIdx}], not setting anyone as speaker.`);
            console.log(this.speakingOrder);
        }
        this.speakerEndsAt = new Date().getTime() + this.intervalSecs * 1000;
        this.broadcastStatus();
        this.timeout = setTimeout(this.nextSpeaker.bind(this), this.intervalSecs * 1000);
    }

    setUserSpeaker(user_id: UserId) {
        this.broadcastMessage('set-main-speaker', user_id);
        //This listener will be unsubscribed after the event has fired once
        //TODO: unsubscribe in nextSpeaker, in case the user hasn't pressed the button
        this.sockets[user_id].once('speakingcircle-next', () => {
            console.log(`Main speaker ${user_id} is done, moving on to next speaker.`);
            if(this.timeout) {
                clearTimeout(this.timeout)
            }
            this.nextSpeaker();
        })
    }
}
