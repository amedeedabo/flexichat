import {Format} from './Format'
/* This Format rotates users into random rooms. There are as
many rooms as users.
The API of Formats isn't set yet, this is a playground
for experimenting with their structure.
 */
export default class RandomRooms extends Format {
    intervalSecs: number;
    intervalObj: ReturnType<typeof setInterval> | undefined;

    constructor(params) {
        super(params);
        this.intervalSecs = params.intervalSecs;
    }

    get name() { return "RandomRooms"}
    get numRooms() {
        return Math.max(Object.keys(this.users).length, 1);
    }
    get status() {
        return {
            name: this.name,
            speakingTime: this.intervalSecs,
            numRooms: this.numRooms,
        }
    }

    userJoined(user_id) {
        this.placeUser(user_id)
    }

    isStarting(_users) {
        this.intervalObj = setInterval(this.mapUsers.bind(this), this.intervalSecs * 1000);
        this.mapUsers();
    }

    isEnding() {
        if(this.intervalObj != undefined) {
            clearInterval(this.intervalObj);
        }
    }

    mapUsers() {
        console.log(`Random rooms: Rotating all the users`)
        Object.keys(this.users).map((id) => {this.placeUser(id)});
    }

    placeUser(id) {
        this.setUserRoom(id, Math.floor(Math.random() * this.numRooms));
    }
}
