import RandomRooms from './formats/RandomRooms';
import SpeakingCirle from './formats/SpeakingCircle';
import ShufflePairs from './formats/ShufflePairs';
import Lobby from './formats/Lobby';
import {Format, PlayState} from './formats/Format';
import {User} from "../server/interfaces"
/*
This class is in charge of running the different formats.
It keeps track of all the connected users in the app.
It sends information about the schedule for the event as users connect,
and switches between formats.


ATM it does its own communication with users through socketio sockets it
keeps, but soon that will be factored out.
*/
interface FormatElem {
    format: Format,
    duration: number,
}
//Cancellable, promise returning version of setTimeout
const later = (delay) => {
    let timer;
    let reject;
    const promise = new Promise((resolve, _reject) => {
        reject = _reject;
        timer = setTimeout(resolve, delay);
    });
    return {
        get promise() { return promise; },
        cancel() {
            if (timer) {
                clearTimeout(timer);
                timer = 0;
                reject();
                reject = null;
            }
        }
    };
};
export default class Schedule {
    static loadedFormats = [RandomRooms, SpeakingCirle, ShufflePairs, Lobby]
    formatIdx: number = -1;
    users: Object = {};
    sockets: any;
    ioServer: any;
    schedule: FormatElem[];
    formatTimeout: ReturnType<typeof setTimeout> | undefined;
    resolvePromise: Function | undefined;

    constructor(conf, ioServer) {
        this.ioServer = ioServer;
        this.schedule = this.buildSchedule(conf.blocks);
        this.sockets = {};
        ioServer.on('connect', socket=> {
            console.log("User connection")
            const session = socket.request.session;
            console.log("Have session")
            console.log(session)
            const user: User = session.user
            console.log("Got user")
            console.log(user);
            this.addUser(user, socket);
            console.log("Added user")
            socket.on('disconnect', _ => {
                if(user) {
                    this.removeUser(user.id);
                    console.log(`User ${user.id} has left.`);
                }
            });
        });
    }
    buildSchedule(formatList) {
        const formatNames = Schedule.loadedFormats.map(f=> f.name);
        return formatList.map(format => {
            const name = format.name;
            const idx = formatNames.indexOf(name);
            if (idx == -1) {
                console.error(`Schedule: Could not load format with name ${name}. Skipping it.`)
                return
            }
            let formatInstance= new Schedule.loadedFormats[idx]({ioServer: this.ioServer, ...format.params});
            return {
                format: formatInstance,
                duration: format.duration
            }
        }).filter(f => f)
    }
    get currentFormat() {
        return this.schedule[this.formatIdx].format;
    }
    get currentDuration() {
        return this.schedule[this.formatIdx].duration;
    }
    start() {
        this.nextFormat();
        return new Promise((resolve, _reject) => {
            this.resolvePromise = resolve;
        });
    }
    end() {
        if (this.resolvePromise != undefined) {
            this.resolvePromise();
        }
    }

    /* Starts the next format.
    Gives the connected users information about the new format.
    Later on we'll give people warning before switching formats.
    Maybe that will be encoded in the Schedule (transitionWarning: 10seconds)
    */
    async nextFormat() {
        this.formatIdx +=1;
        if (this.formatIdx >= this.schedule.length) {
            this.end();
            return;
        }
        const formatEnds = this.currentFormat.start(this.users, this.sockets)?.then(() => {
            console.log("Schedule: Format is choosing to end early.");
        })
        console.log(`Setting format to run out in ${this.currentDuration }`)

        const formatTimesOut = later(this.currentDuration * 1000);
        const currentFormat = this.currentFormat; //I'm capturing this var to make sure we .end() the right format
        const timePromise = formatTimesOut.promise.then(() => {
            console.log(`Schedule: Format has run for its duration of ${this.currentDuration} seconds.`);
            currentFormat.end();
        })
        await Promise.race([formatEnds, timePromise])
        if (this.currentFormat.active_state == PlayState.PLAYING) {
            console.log("Format ended early, cancelling setTimeout for calling end on it.")
            formatTimesOut.cancel();
        }
        this.nextFormat();
    }
    endedEarly() {
        if (this.formatTimeout) {
            clearTimeout(this.formatTimeout);
        }
        //Later we can check if this format has a starting time and wait until then
        this.nextFormat();
    }
    addUser(user: User, socket) {
        if(!user) {
            console.error("Adding user but object is empty")
            return
        }
        this.users[user.id] = user;
        this.sockets[user.id] = socket;
        this.broadcastPresence()
        this.currentFormat.addUser(user, socket)
    }
    broadcastPresence() {
        console.log("Broadcasting presence")
        console.log(this.users)
        this.ioServer.emit('presence', this.users)
        console.log("Done broadcasting presence")
    }
    removeUser(user_id) {
		this.currentFormat.removeUser(user_id);
		delete this.users[user_id];
    }
}
