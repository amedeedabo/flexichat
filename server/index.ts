import { createServer } from 'http';
import next from 'next';
import express from 'express';
import socketio from 'socket.io';
import session from 'express-session';
import { setupEvents } from './Event';

const app = express();
const server = createServer(app);
const io = socketio(server)

const port = parseInt(process.env.PORT || '3000', 10)
const dev = process.env.NODE_ENV !== 'production'
const nextApp = next({ dev })
const nextHandler = nextApp.getRequestHandler()

//This sets up the session middleware, which will look up the user's
//session information based on their session cookie, and add everything
//stored in the session to req.session for downstream request handlers to see.
var sessionMiddleware = session({
    secret: "todo",
});

//Add the session Middleware to express
app.use(sessionMiddleware);
//Add the session middleware to socketio
io.use(function(socket, next) {
	sessionMiddleware(socket.request, {}, next);
});



nextApp.prepare().then(async () => {
	app.get('*', (req, res) => {
		return nextHandler(req, res);
	});

	server.listen(port);
	// tslint:disable-next-line:no-console
	console.log(`> Server listening at http://localhost:${port} as ${
		dev ? 'development' : process.env.NODE_ENV }`);
	await setupEvents(io);
});
