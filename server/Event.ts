import Schedule from './Schedule';
import {getFutureEvents, updateEvent} from "../db/events"
import { RRule } from "rrule"
import {Event} from "server/interfaces"

const runAt = (fn: Function, date: Date) => setTimeout(fn, date.getTime() - Date.now());

function scheduleEvent(ioServer, event: Event) {
	console.log(`Lining up ${event.id} to start at ${event.starts_at}`)
	runAt(() => runEvent(ioServer, event), new Date(event.starts_at))
}

export async function setupEvents(io) {
	for (const event of await getFutureEvents()) {
		scheduleEvent(io, event)
	}
}

async function runEvent(ioServer, event: Event) {
	console.log(`Starting event ${event.id}:`);
	//Uses a custom namespace(https://socket.io/docs/rooms-and-namespaces/) for each event so that
	//users in separate events don't receive the same messages.
	const sched = new Schedule(event, ioServer.of(`/${event.id}`))
	await sched.start();

	console.log(`${event.id} has ended.`)
	if (event.rrule != undefined) {
		const next_start: string = RRule.fromString(event.rrule).after(new Date()).toString()
		console.log(`${event.id} is a recurring event with rrule ${event.rrule}, setting its next start .`)
		updateEvent(event.id, {"starts_at": next_start})
		event.starts_at = next_start //hack, updateEvent should return us a new event from the DB
		scheduleEvent(ioServer, event)
	}
}