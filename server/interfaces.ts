export type UserId = number;
export type User = {
    id: UserId,
    email: string | undefined,
    alias: string,
}
export interface Users {
    [id: string]: User;
}
export interface FormatConfig {
        name: string,
        duration: number, //duration in seconds
        params?: any,
}
export interface Event {
		id: string,
		name: string,
		starts_at: string,
		rrule?: string,
		blocks: FormatConfig[]
}
