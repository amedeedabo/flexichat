export const sessionOptions = {
    name: "sid", //todo: make it a fancy Host only prefix,
    cookie: {
        sameSite: "lax",
        secure: true,
        httpOnly: true,
        path: "/",
        domain: null
    }
};