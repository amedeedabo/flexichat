import style from "./NavBar.module.scss";
const NavBar = () => (
    <div className={style.NavBar}>
      <p>Join us hacking on it <a href="https://gitlab.com/amedeedabo/flexichat">on Gitlab</a>, or reach out to me: <a href="https://twitter.com/rockmeamedee">@rockmeamedee</a></p>
    </div>
  );

  export default NavBar;
