import React, { useState, useEffect } from 'react'
import dynamic from 'next/dynamic'

const FormatStatus = (props) => {
    let formatName = props.name || "RandomRooms";
    let [collapsed, setCollapsed] = useState(false)
    //TODO: sanitize
    const DynamicComponent = dynamic(() => import(`../components/Statuses/${formatName}.js`),
        { loading: () => (<p>Loading the format status.</p>),
            ssr: false
        })
    return (<div>
            <button  onClick={() => setCollapsed(collapsed => !collapsed)}>
                {collapsed ? ">>" : "<<"}
            </button>
            {!collapsed && (<div>
                <h3>{formatName}</h3>
                <DynamicComponent {...props} numRooms={5} intervalSecs={10}/>
            </div>)}
        </div>)
}
export default FormatStatus;