import Head from 'next/head'
import Header from "./Header";
import NavBar from "./NavBar";
import style from "./Layout.module.scss";

const Layout = props => (
  <div className={style.Layout}>
    <Head>
        <title>Flexichat</title>
    </Head>
    <Header />
    {props.children}
  </div>
);

export default Layout;