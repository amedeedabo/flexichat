import { useState, useEffect } from "react";

const calculateTimeLeft = (until) => {
    const difference = until - +(new Date().getTime());
    let timeLeft = {};

    if (difference > 0) {
        timeLeft = {
            days: Math.floor(difference / (1000 * 60 * 60 * 24)),
            hours: Math.floor((difference / (1000 * 60 * 60)) % 24),
            minutes: Math.floor((difference / 1000 / 60) % 60),
            seconds: Math.floor((difference / 1000) % 60)
        };
    }
    return timeLeft;
};

export default function Countdown(props) {
    let {end} = props
    const [timeLeft, setTimeLeft] = useState(calculateTimeLeft(end));
    useEffect(() => {
        setTimeout(() => {
            setTimeLeft(calculateTimeLeft(end))
        }, 1000)
    })

    const timerComponents = Object.keys(timeLeft).map(interval => {
        if (!timeLeft[interval]) {
            return;
        }
        return (
            <span key={`countdown-${interval}`}>
                {timeLeft[interval]} {interval}{" "}
            </span>
        );
    });

    return (
        <>
            {timerComponents.length ? timerComponents : <span>Time's up!</span>}
        </>
    );
}