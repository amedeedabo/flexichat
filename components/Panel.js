import style from "./Panel.module.scss";
const Panel = props => (
    <div className={style.Panel} style={{backgroundColor: props.background}}>
        <div className={style.PanelContent}>
            {props.children}
        </div>
    </div>
  );

  export default Panel;
