import style from "./Schedule.module.scss";
import { useState } from "react";
import moment from "moment";
const Schedule = props => {
    let [collapsed, setCollapsed] = useState(false)
    let starts_at = props.sched?.starts_at;
    let blocks = props.sched?.blocks || [
        {name: "Lobby", duration: 2 * 60},
        {name: "SpeakingCircle", duration: 3 * 2 * 60},
        {name: "Lobby", duration: 7 * 60},
    ]
    return (
    <div className={style.Schedule}>
        <button className={style.collapse} onClick={() => setCollapsed(collapsed => !collapsed)}>
            {collapsed ? ">>" : "<<"}
            </button>
        {!collapsed && props.sched && (<div>
            <h3>Schedule</h3>
            <h6>Starts at {starts_at}</h6>
            <ul>
                {blocks.map((format, idx) => {
                    return <li key={idx}>{format.name}: {moment.duration(format.duration, 'seconds').humanize()}</li>
                })
                }
            </ul>
        </div>)}
    </div>)
  };

export default Schedule;