import moment from "moment";
const ShufflePairs = (props) => {
    const {currentMatches, intervalSecs, presence} = props;
    return (<>
            <h5>Everyone is being paired 1 on 1 for {moment.duration(intervalSecs, 'seconds').humanize()}</h5>
            {currentMatches && (<ul>
                {currentMatches.map((match,idx,_) => (<li key={`info-${idx}`}>{match.map(m => presence[m].alias).join(" is matched with ")}</li>))}
            </ul>
            )}
        </>
    )
}

export default ShufflePairs