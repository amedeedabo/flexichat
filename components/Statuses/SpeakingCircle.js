import Countdown from '../Countdown.tsx'
import UserList from '../UserList.tsx'
import moment from "moment";
//TODO: make the background change when you are the main speaker
const SpeakingCircle = (props) => {
    const {interval, speakingOrder, mainSpeaker, speakerEndsAt, user, presence} = props;
    const uid = user.id
    const isMainSpeaker = (mainSpeaker == uid);
    //TODO: display the speaking interval correctly 'humanely' with moment.
    return (
        <div>
            <h5>Everyone speaks for {moment.duration(interval, 'seconds').humanize()}.</h5>
            <p>The speaking order is: </p>
            <UserList users={speakingOrder.map(id => presence[id])} />
            {isMainSpeaker ? (<>
                    <p>You are the main speaker!</p>
                 </>) :
                (<p>{presence[mainSpeaker]?.alias || mainSpeaker} is currently speaking:</p>)
            }
            <Countdown end={speakerEndsAt}/>
            <br/>
            {isMainSpeaker && <button onClick={() => props.socket.emit('speakingcircle-next')}>I'm Done Speaking</button>}
        </div>
    )
}

export default SpeakingCircle