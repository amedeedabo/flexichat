const RandomRooms = (props) => {
    return (
        <div>
            <h5>Participants are randomly placed in one of {props.numRooms} rooms.</h5>
            <p>The rooms are rotated every <b>{props.intervalSecs}</b> seconds.</p>
        </div>
    )
}

export default RandomRooms