import UserList from '../UserList';
const Lobby = (props) => {
    const {presence} = props;
    return (<>
            {presence ?
                (<><h5>The following people are in the lobby:</h5><UserList users={presence} /></>) :
                (<h5>There is no one in the lobby yet.</h5>)
            }
        </>
    )
}

export default Lobby