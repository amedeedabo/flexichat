import style from "./Header.module.scss";
import Link from 'next/link'

const Header = () => (
<div className="Header" className={style.Header}>
    <Link href="/"><a>flexi.chat</a></Link>
</div>
);

export default Header;