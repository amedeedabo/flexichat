import {Users} from "../server/interfaces"
const UserList = (props) => {
    const users: Users = props.users;
    return (
        <ul>
            {Object.values(users).map(u=> (<li key={u.id}>{u.alias}</li>))}
        </ul>
    )
}

export default UserList;