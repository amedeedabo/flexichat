import React, { useState, useEffect, useRef, forwardRef } from 'react'
import PropTypes from 'prop-types'

const Jutsu = (props) => {
  const { domain, roomName, displayName, password, subject, registerJitsiCommands } = props
  const { loadingComponent, containerStyles, jitsiContainerStyles } = props

  const [loading, setLoading] = useState(true)
  //This is all the default buttons, minus the 'info' one, so that the "Share"
  //modal doesn't pop up when joining a room.
  let interfaceConfigOverwrite = {
    TOOLBAR_BUTTONS: [
      'microphone', 'camera', 'closedcaptions', 'desktop', 'fullscreen',
      'fodeviceselection', 'hangup', 'profile', 'chat', 'recording',
      'livestreaming', 'etherpad', 'sharedvideo', 'settings', 'raisehand',
      'videoquality', 'filmstrip', 'invite', 'feedback', 'stats', 'shortcuts',
      'tileview', 'videobackgroundblur', 'download', 'help', 'mute-everyone',
      'e2ee'
    ]
  }
  const parentNode = useRef(null);//'jitsi-container';
  const [jitsi, setJitsi] = useState(null)

  useEffect(() => {
    if (window.JitsiMeetExternalAPI) {
      const api = new JitsiMeetExternalAPI(domain || 'meet.jit.si', { interfaceConfigOverwrite, roomName, parentNode: parentNode.current });
      setJitsi(api);
      props.onLoad?.(api);
    } else {
      const error = 'JitsiMeetExternalAPI is not available, check if https://meet.jit.si/external_api.js was loaded';
      setJitsi({ error });
      console.error(error);
    }
    return () => jitsi && jitsi.dispose()
  }, [roomName])

  const containerStyle = {
    //  width: '900px',
    //height: '500px'
    height: "100%"
  }

  const jitsiContainerStyle = {
    display: loading ? 'none' : 'block',
    width: '100%',
    height: '100%'
  }

  useEffect(() => {
    if (jitsi) {
      jitsi.addEventListener('videoConferenceJoined', () => {
        if (password) jitsi.executeCommand('password', password)
        jitsi.executeCommand('displayName', displayName)
        jitsi.executeCommand('subject', subject)
      })
      setLoading(false)
    }
    return () => jitsi && jitsi.dispose()
  }, [jitsi])

  return (
    <div style={{ ...containerStyle, ...containerStyles }}>
      {loading && (loadingComponent || <p>Loading ...</p>)}
      <div
        ref={parentNode}
        style={{ ...jitsiContainerStyle, ...jitsiContainerStyles }}
      />
    </div>
  )
}

Jutsu.propTypes = {
  domain: PropTypes.string,
  roomName: PropTypes.string.isRequired,
  displayName: PropTypes.string,
  password: PropTypes.string,
  subject: PropTypes.string,
  loadingComponent: PropTypes.object,
  containerStyles: PropTypes.object,
  jitsiContainerStyles: PropTypes.object
}

const Jitsi = forwardRef((props, ref) => {
  const {room, name} = props;
  const [call, setCall] = useState(false)

  const handleClick = event => {
    event.preventDefault()
    if (name) setCall(true)
  }

  return (<div id="jitsi-div" style={{ flex: 1, height: "auto" }}>
    {call && <Jutsu
      roomName={room}
      onLoad={props.onJitsiLoad}
      displayName={name}
      loadingComponent={<p>loading ...</p>} />
    }
    <form>
      <button onClick={handleClick} type='submit'>
        Join Video
      </button>
    </form>
  </div>
  )
})
export default Jitsi